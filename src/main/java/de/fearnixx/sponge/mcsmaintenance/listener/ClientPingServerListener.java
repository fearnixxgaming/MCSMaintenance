package de.fearnixx.sponge.mcsmaintenance.listener;

import de.fearnixx.sponge.mcsmaintenance.IMCSMaintenance;
import de.fearnixx.sponge.mcsmaintenance.IMaintenanceManager;
import de.fearnixx.sponge.mcsmaintenance.set.IMaintenanceSet;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.server.ClientPingServerEvent;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

/**
 * Created by Life4YourGames on 24.11.16.
 */
public class ClientPingServerListener {

    private IMCSMaintenance plugin;

    public ClientPingServerListener(IMCSMaintenance plugin) {
        this.plugin = plugin;
    }

    @Listener
    public void onAdvClientPingServerEvent(ClientPingServerEvent event) {
        Optional<IMaintenanceManager> maintMananger = Sponge.getServiceManager().provide(IMaintenanceManager.class);
        if (!maintMananger.isPresent()) {
            plugin.getLogger().warn("Unable to process CPSE: Maintenance-Manager not available");
            event.getResponse().setDescription(Text.of(TextColors.RED, "Unavailable due to maintenance"));
            return;
        }
        if (!maintMananger.get().isEnabled()) return;

        List<IMaintenanceSet> sets = maintMananger.get().getActiveSets();
        Optional<IMaintenanceSet> set = sets.stream()
                .filter(s -> s.getMOTD().isPresent())
                .max(Comparator.comparingInt(IMaintenanceSet::getPriority));
        if (set.isPresent()) {
            event.getResponse().setDescription(set.get().getMOTD().get());
            plugin.getLogger().debug("MOTD changed");
        } else {
            plugin.getLogger().debug("MOTD unchanged");
        }
    }
}
