package de.fearnixx.sponge.mcsmaintenance.listener;

import de.fearnixx.sponge.mcsmaintenance.IMCSMaintenance;
import de.fearnixx.sponge.mcsmaintenance.IMaintenanceManager;
import de.fearnixx.sponge.mcsmaintenance.MCSMaintenancePermissions;
import de.fearnixx.sponge.mcsmaintenance.set.IMaintenanceSet;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.service.permission.PermissionService;
import org.spongepowered.api.service.permission.SubjectReference;
import org.spongepowered.api.service.user.UserStorageService;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.chat.ChatTypes;
import org.spongepowered.api.text.format.TextColors;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

/**
 * Handles allowJoin restrictions and server lock down in case something initialized wrongly.
 */
public class ServerRestrictPlayerAuthListener {

    private IMCSMaintenance plugin;
    private Logger logger;
    public ServerRestrictPlayerAuthListener(IMCSMaintenance plugin) {
        this.plugin = plugin;
        this.logger = plugin.getLogger();
    }

    @Listener
    public void onPlayerJoinEvent(ClientConnectionEvent.Auth event) {
        Text LOC_MESSAGE;
        Optional<IMaintenanceManager> maintManager = Sponge.getServiceManager().provide(IMaintenanceManager.class);

        if (!maintManager.isPresent()) {
            plugin.getLogger().warn("Unable to process CCE.Auth: Maintenance-Manager not active");
            event.setCancelled(true);
            event.setMessage(Text.of(TextColors.RED, "Cannot verify login-clearances: Server locked down."));
            return;
        }
        if (!maintManager.get().isEnabled()) return;

        Optional<UserStorageService> storageService = Sponge.getServiceManager().provide(UserStorageService.class);

        if (!storageService.isPresent()) {
            plugin.getLogger().error("Storage Service unavailable! Unable to check connecting user!");
            event.setCancelled(true);
            event.setMessage(Text.of(TextColors.RED, "Cannot verify login-clearances: Server locked down"));
            return;
        }

        Optional<User> user = storageService.get().get(event.getProfile().getUniqueId());
        String uid = event.getProfile().getUniqueId().toString();
        String name = event.getProfile().getName().orElse("unknown");
        if (!user.isPresent()
                && maintManager.get().getActiveSets().stream().anyMatch(IMaintenanceSet::restrictsJoin)) {

            // Dig deeper by asking the permission service directly
            Optional<PermissionService> optPermService = Sponge.getServiceManager().provide(PermissionService.class);
            if (optPermService.isPresent()) {
                try {
                    Map<SubjectReference, Boolean> collection = optPermService.get().getUserSubjects().getAllWithPermission(MCSMaintenancePermissions.MAINT_EXEMPT_JOIN).get();
                    Boolean[] result = new Boolean[]{Boolean.FALSE};
                    collection.entrySet()
                              .stream()
                              .filter(entry -> entry.getKey().getSubjectIdentifier().equals(uid) && entry.getValue())
                              .findFirst()
                              .ifPresent(found -> result[0] = Boolean.TRUE);

                    // The subject appears to have the exempt permission. Allow join but log.
                    if (result[0]) {
                        logger.info("AllowJoin exemption by deep search for: " + uid + "/" + name);
                        return;
                    }
                } catch (InterruptedException|ExecutionException e) {
                    logger.info("AllowJoin deep search failed for: " + uid + "/" + name);
                    logger.debug("Exception:", e);
                }
            }
            //Check if player has been exempted
            if (maintManager.get().playerRequestExemption(event.getProfile().getName().orElse(null))) return;

            event.setCancelled(true);
            event.setMessage(Text.of(TextColors.RED, "Missing in database. (First connect?)", TextColors.WHITE, "\n Please return when maintenance is disabled"));

            Text message = Text.of(TextColors.RED, "Player \"", Text.of(TextActions.showText(Text.of(uid)), TextActions.suggestCommand("/"+uid), name), "\" denied access to the server: Missing in database.");
            Sponge.getServer().getBroadcastChannel().send(message, ChatTypes.SYSTEM);
            return;
        }

        maintManager.get().getActiveSets().stream()
                .filter(s -> !s.checkPlayerAgainstServer(user.get()))
                .findFirst()
                .ifPresent(s -> {
                    //User's not allowed
                    event.setCancelled(true);
                    event.setMessage(Text.of(TextColors.RED, "Login rejected: Server is undergoing maintenance."));
                    Text message = Text.of(TextColors.RED, "Player \"", Text.of(TextActions.showText(Text.of(uid)), TextActions.suggestCommand("/"+uid), name), "\" denied access to the server: Undergoing maintenance.");
                    Sponge.getServer().getBroadcastChannel().send(message, ChatTypes.SYSTEM);
                });
    }
}
