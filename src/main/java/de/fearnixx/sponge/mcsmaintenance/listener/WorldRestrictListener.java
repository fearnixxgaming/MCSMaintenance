package de.fearnixx.sponge.mcsmaintenance.listener;

import de.fearnixx.sponge.mcsmaintenance.IMCSMaintenance;
import de.fearnixx.sponge.mcsmaintenance.IMaintenanceManager;
import de.fearnixx.sponge.mcsmaintenance.MCSMaintenancePermissions;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.Transform;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.entity.MoveEntityEvent;
import org.spongepowered.api.event.entity.living.humanoid.player.RespawnPlayerEvent;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import java.util.Optional;

/**
 * Created by MarkL4YG on 10-Mar-18
 */
public class WorldRestrictListener {

    private IMCSMaintenance plugin;

    public WorldRestrictListener(IMCSMaintenance plugin) {
        this.plugin = plugin;
    }

    private boolean playerAllowedIn(Player player, World world) {
        return player.hasPermission(MCSMaintenancePermissions.MAINT_EXEMPT_WORLDS)
                || player.hasPermission(MCSMaintenancePermissions.MAINT_EXEMPT_WORLD + "." + world.getName());
    }

    private Transform<World> checkPlayer(Player player, Transform<World> to, IMaintenanceManager maintManager) {
        //Do not restrict default world!
        if (to.getExtent().getName().equals(Sponge.getServer().getDefaultWorldName())) return null;

        boolean hasExemptPermission = playerAllowedIn(player, to.getExtent());
        Optional<Transform<World>>[] t = new Optional[]{null};
        if (maintManager.getActiveSets()
                        .stream()
                        .anyMatch(set -> {
            t[0] = set.checkPlayerAgainstWorld(player, to, null);
            return t[0].isPresent();
        })) {
            if (hasExemptPermission) {
                player.sendMessage(Text.of(TextColors.YELLOW, "Notice: This world is currently in maintenance."));
                return null;
            } else if (maintManager.playerRequestExemption(player)) {
                player.sendMessage(Text.of(TextColors.YELLOW, "Notice: You have been exempted from a maintenance permission check."));
                player.sendMessage(Text.of(TextColors.YELLOW, "Notice: This world is currently in maintenance."));
                return null;
            }
            return t[0].get();
        }
        return null;
    }

    /**
     * Handles world maintenance upon PlayerJoinEvent
     */
    @Listener
    public void onPlayerJoin(ClientConnectionEvent.Join event) {
        Optional<IMaintenanceManager> maintManager = Sponge.getServiceManager().provide(IMaintenanceManager.class);
        if (!maintManager.isPresent()) {
            plugin.getLogger().warn("Unable to process CCE.Join: Maintenance-Manager not active");
            return;
        }
        if (!maintManager.get().isEnabled()) return;

        Player player = event.getTargetEntity();
        Transform<World> to = player.getTransform();
        Transform<World> forcedTo = checkPlayer(player, to, maintManager.get());

        if (forcedTo != null) {
            //User's redirected
            player.setTransform(forcedTo);
            player.sendMessage(Text.of(TextColors.RED, "CCE.Join: The world you were trying to enter is currently in maintenance, you've been redirected."));
        }
    }

    /**
     * Handles world maintenance upon PlayerRespawnEvent
     */
    @Listener
    public void onPlayerRespawn(RespawnPlayerEvent event) {
        Optional<IMaintenanceManager> maintManager = Sponge.getServiceManager().provide(IMaintenanceManager.class);
        if (!maintManager.isPresent()) {
            plugin.getLogger().warn("Unable to process PRE: Maintenance-Manager not active");
            return;
        }
        if (!maintManager.get().isEnabled()) return;

        Player player = event.getTargetEntity();
        Transform<World> to = player.getTransform();
        Transform<World> forcedTo = checkPlayer(player, to, maintManager.get());

        if (forcedTo != null) {
            //User's redirected
            player.setTransform(forcedTo);
            player.sendMessage(Text.of(TextColors.RED, "CCE.Join: The world you were trying to enter is currently in maintenance, you've been redirected."));
        }
    }

    /**
     * Handles world maintenance upon PlayerTeleportEvent
     */
    @Listener
    public void onPlayerTeleport(MoveEntityEvent.Teleport event) {
        Text LOC_MESSAGE;
        Optional<IMaintenanceManager> maintManager = Sponge.getServiceManager().provide(IMaintenanceManager.class);
        if (!maintManager.isPresent()) {
            plugin.getLogger().warn("Unable to process PTE: Maintenance-Manager not active");
            return;
        }

        if (!maintManager.get().isEnabled()) return;

        if (!(event.getTargetEntity() instanceof Player)) return;
        Player player = (Player) event.getTargetEntity();
        Transform<World> to = event.getToTransform();
        Transform<World> from = event.getFromTransform();

        // To not restrict teleportation within a world.
        if (to.getExtent().getUniqueId().equals(from.getExtent().getUniqueId()))
            return;

        Transform<World> forcedTo = checkPlayer(player, to, maintManager.get());
        if (forcedTo != null) {
            //User's redirected
            player.setTransform(forcedTo);
            player.sendMessage(Text.of(TextColors.RED, "CCE.Join: The world you were trying to enter is currently in maintenance, you've been redirected."));
        }
    }
}
