package de.fearnixx.sponge.mcsmaintenance;

import com.google.common.reflect.TypeToken;
import de.fearnixx.sponge.mcsmaintenance.commands.MaintCommand;
import de.fearnixx.sponge.mcsmaintenance.info.FNGBasicMaintenanceSet;
import de.fearnixx.sponge.mcsmaintenance.listener.ClientPingServerListener;
import de.fearnixx.sponge.mcsmaintenance.listener.ServerRestrictPlayerAuthListener;
import de.fearnixx.sponge.mcsmaintenance.listener.WorldRestrictListener;
import de.fearnixx.sponge.mcsmaintenance.set.IMaintenanceSet;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.Transform;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import java.util.*;

/**
 * Created by Life4YourGames on 20.03.17.
 */
public class FNGMaintenanceManager implements IMaintenanceManager {

    private IMCSMaintenance plugin;
    private ConfigurationNode config;
    private boolean enabled;

    private Map<String, Integer> playerExemptions;

    private List<IMaintenanceSet> activeSets;
    private Map<String, IMaintenanceSet> setsAvailable;
    private ServerRestrictPlayerAuthListener playerAuthListener;
    private WorldRestrictListener worldRestrictListener;
    private ClientPingServerListener clientPingServerListener;

    public FNGMaintenanceManager(MCSMaintenance plugin) {
        enabled = false;
        this.plugin = plugin;
        playerExemptions = new HashMap<String, Integer>();
        activeSets = new ArrayList<IMaintenanceSet>();
        setsAvailable = new HashMap<String, IMaintenanceSet>();
        playerAuthListener = new ServerRestrictPlayerAuthListener(plugin);
        worldRestrictListener = new WorldRestrictListener(plugin);
        clientPingServerListener = new ClientPingServerListener(plugin);
    }

    private void initConfig(ConfigurationNode c) {
        ConfigurationNode node;
        //Enable
        node = c.getNode("enabled");
        if (node.isVirtual()) node.setValue(true);
    }

    public boolean initialize() {
        enabled = false;
        playerExemptions.clear();
        activeSets.clear();
        setsAvailable.clear();

        config = plugin.getConfig().getNode("features", "maintenance");
        if (config==null) return false;
        initConfig(config);

        plugin.getLogger().debug("Registering listeners");
        Sponge.getEventManager().registerListeners(plugin, playerAuthListener);
        Sponge.getEventManager().registerListeners(plugin, worldRestrictListener);
        Sponge.getEventManager().registerListeners(plugin, clientPingServerListener);

        ConfigurationNode setsNode = config.getNode("sets");
        setsNode.getChildrenMap().forEach((k, v) -> {
            FNGBasicMaintenanceSet s = new FNGBasicMaintenanceSet(setsNode.getNode(k), plugin);
            if (s.getName().isEmpty() || s.getName().equals("null")) return;
            setsAvailable.put(s.getName(), s);
        });

        enabled = config.getNode("enabled").getBoolean(true);
        if (enabled) {
            Sponge.getCommandManager().register(plugin, MaintCommand.commandSpec(plugin), "maint");
        } else {
            plugin.getLogger().warn("Maintenance disabled.");
        }

        //TODO: Events
        plugin.getLogger().debug("Searching for previously enabled maintenance sets");
        ConfigurationNode activeSetsNode = config.getNode("active");
        if (!activeSetsNode.isVirtual()) {
            List<String> l = null;
            try {
                l = activeSetsNode.getList(TypeToken.of(String.class));
            } catch (ObjectMappingException e) {
                //
            }
            if (l!=null && l.size()>0) {
                l.forEach(n -> {
                    plugin.getLogger().debug("Enabling set: " + n);
                    //enableInfo(n);
                    if (!enableSet(n)) plugin.getLogger().warn("Unable to enable set: " + n);
                });
            }
        }

        if (activeSets.size() > 0) {
            plugin.getLogger().info(activeSets.size() + " sets enabled.");
        } else {
            plugin.getLogger().info("No sets enabled on startup");
        }
        if (enabled) Sponge.getServiceManager().setProvider(plugin, IMaintenanceManager.class, this);
        return enabled;
    }

    public boolean isEnabled() { return enabled; }

    public boolean save() {
        List<String> enabledSets = new ArrayList<String>();
        activeSets.forEach(set -> enabledSets.add(set.getName()));
        config.getNode("active").setValue(enabledSets);
        return true;
    }

    //--- --- --- --- --- --- --- --- --- --- --- MaintenanceSets --- --- --- --- --- --- --- --- --- --- --- --- ---

    public boolean registerSet(IMaintenanceSet set) {
        return setsAvailable.putIfAbsent(set.getName(), set) == null;
    }

    public void removeSet(IMaintenanceSet set) {
        disableSet(set);
        setsAvailable.remove(set.getName());
    }

    public boolean enableSet(String setName) {
        return setsAvailable.containsKey(setName) && enableSet(setsAvailable.get(setName));
    }

    public boolean enableSet(IMaintenanceSet set) {
        if (activeSets.stream().anyMatch(s -> s == set)) {
            return false;
        }
        activeSets.add(set);
        Text kickMessage = Text.of(TextColors.RED, "The server is now undergoing maintenance. Please come back later.");
        Text moveMessage = Text.of(TextColors.RED, "The world you were in is now in maintenance. You have been moved.");
        if (set.enforceOnEnable()) {
            Sponge.getServer().getOnlinePlayers().parallelStream()
                    .filter(player -> !set.checkPlayerAgainstServer(player))
                    .forEach(player -> player.kick(kickMessage));
            Sponge.getServer().getOnlinePlayers().parallelStream()
                    .forEach(player -> {
                        Optional<Transform<World>> w = set.checkPlayerAgainstWorld(player, player.getTransform(), null);
                        if (!w.isPresent()) return;
                        player.setLocation(w.get().getLocation());
                        player.sendMessage(moveMessage);
                    });
        }
        return true;
    }

    public boolean disableSet(String setName) {
        IMaintenanceSet[] s = new IMaintenanceSet[]{null};
        activeSets.stream().anyMatch(set -> {
            if (setName.equals(set.getName())) {
                s[0] = set;
                return true;
            }
            return false;
        });
        return disableSet(s[0]);
    }

    public boolean disableSet(IMaintenanceSet set) {
        return activeSets.remove(set);
    }

    public void reloadSet(String setName) {
        IMaintenanceSet s = setsAvailable.getOrDefault(setName, null);
        if (s != null) s.reload();
    }

    public Optional<IMaintenanceSet> getSet(String setName) {
        return Optional.ofNullable(setsAvailable.getOrDefault(setName, null));
    }

    public List<IMaintenanceSet> getActiveSets() { return activeSets; }
    public Map<String, IMaintenanceSet> getAvailableSets() { return setsAvailable; }

    //--- --- --- --- --- --- --- --- --- --- --- Player Exemptions --- --- --- --- --- --- --- --- --- --- --- --- ---

    /**
     * Exempt a player from the next \<exemptCount\> maintenance checks
     * If the player is already exempted, the amount is replaced
     * If exemptCount is <=0 the player entry is removed
     * @param name The player to exempt
     * @param exemptCount How many times to exempt
     * @return Whether the player can still be exempted!
     *         ATTENTION: For the sake of #playerRequestExemption this method returns true for exemptCount>=0
     *         Keep in mind that setting the exemptCount to 0 !wouldn't! allow an exemption
     * @see #playerRequestExemption(User)
     */
    public boolean playerSetExemption(String name, int exemptCount) {
        if (!isEnabled()) return false;
        boolean res = false;
        if (exemptCount>=0) {
            res = true;
        }
        if (exemptCount>0) {
            if (playerExemptions.containsKey(name))
                playerExemptions.replace(name, exemptCount);
            else
                playerExemptions.put(name, exemptCount);
        } else {
            playerExemptions.remove(name);
        }
        return res;
    }

    /**
     * Exempt a player from the next \<exemptCount\> maintenance checks
     * If the player is already exempted, the amount is summed up
     * @param playerName The player to exempt
     * @param exemptCount How many times to exempt
     * @return Total amount of exemptions that will be done
     */
    public int playerAddExemption(String playerName, int exemptCount) {
        if (!isEnabled()) return 0;
        int total = 0;
        if (playerExemptions.containsKey(playerName)) {
            total = Integer.sum(exemptCount, playerExemptions.get(playerName));
            playerExemptions.replace(playerName, total);
        } else {
            total = exemptCount;
            playerExemptions.put(playerName, exemptCount);
        }
        if (total<=0) {
            playerExemptions.remove(playerName);
        }
        return total;
    }

    public boolean playerRequestExemption(String name) {
        if (!isEnabled()) return false;
        if (playerExemptions.containsKey(name)) {
            int count  = playerExemptions.get(name);
            return playerSetExemption(name, count-1);
        }
        return false;
    }

    /**
     * Request an exemption for \<user\>.
     * This will lower his count by 1
     * @param user The user to exempt (by name)
     * @return Whether an exemption is allowed or not
     */
    public boolean playerRequestExemption(User user) {
        return isEnabled() && playerRequestExemption(user.getName());
    }

    //--- --- --- --- --- --- --- --- --- --- --- Misc  --- --- --- --- --- --- --- --- --- --- --- --- ---

    public ConfigurationNode getConfig() { return config; }
}
