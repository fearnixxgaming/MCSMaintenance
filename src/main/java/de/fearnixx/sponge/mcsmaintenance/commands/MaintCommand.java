package de.fearnixx.sponge.mcsmaintenance.commands;

import de.fearnixx.sponge.mcsmaintenance.IMCSMaintenance;
import de.fearnixx.sponge.mcsmaintenance.MCSMaintenancePermissions;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

/**
 * /maint command implementation for managing maintenance sets
 */
public class MaintCommand implements CommandExecutor {

    public static final String CHATDIV = new String(new char[40]).replace('\0', '+') + "\n";

    public static CommandSpec commandSpec(IMCSMaintenance plugin) {
        return CommandSpec.builder()
                .description(Text.of("Main maintenance commands"))
                .arguments(
                        GenericArguments.none()
                )
                .permission(MCSMaintenancePermissions.MAINT_COMMAND)
                .child(MaintToggleCommand.commandSpec(plugin), "toggle")
                .child(MaintDelCommand.commandSpec(plugin), "remove", "del", "rm", "delete")
                .child(MaintCreateCommand.commandSpec(plugin), "create", "cr", "mk", "make", "add")
                .child(MaintEditCommand.commandSpec(plugin), "edit")
                .child(MaintListCommand.commandSpec(plugin), "list")
                .child(MaintShowCommand.commandSpec(plugin), "info", "show")
                .child(MaintExemptCommand.commandSpec(plugin), "exempt", "pardon")
                .build();
    }

    private IMCSMaintenance plugin;

    public MaintCommand(IMCSMaintenance plugin) {
        this.plugin = plugin;
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext ctx) throws CommandException {
        src.sendMessage(Text.of(TextColors.RED, "Unknown command: "));
        return CommandResult.empty();
    }
}
