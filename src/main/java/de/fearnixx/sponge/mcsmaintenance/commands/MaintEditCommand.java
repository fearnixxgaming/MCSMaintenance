package de.fearnixx.sponge.mcsmaintenance.commands;

import com.google.common.reflect.TypeToken;
import de.fearnixx.sponge.mcsmaintenance.IMCSMaintenance;
import de.fearnixx.sponge.mcsmaintenance.IMaintenanceManager;
import de.fearnixx.sponge.mcsmaintenance.MCSMaintenancePermissions;
import de.fearnixx.sponge.mcsmaintenance.info.FNGBasicMaintenanceSet;
import de.fearnixx.sponge.mcsmaintenance.set.IMaintenanceSet;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.serializer.TextSerializers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Life4YourGames on 24.11.16.
 */
public class MaintEditCommand implements CommandExecutor {

    public static CommandSpec commandSpec(IMCSMaintenance plugin) {
        return CommandSpec.builder()
                .description(Text.of("Edit a maintenance set"))
                .arguments(
                        GenericArguments.string(
                                Text.of("name")
                        ),
                        GenericArguments.string(
                                Text.of("key")
                        ),
                        GenericArguments.string(
                                Text.of("value")
                        )
                )
                .executor(new MaintEditCommand(plugin))
                .permission(MCSMaintenancePermissions.MAINT_SET_EDIT)
                .build();
    }

    private IMCSMaintenance plugin;

    public MaintEditCommand(IMCSMaintenance plugin) {
        this.plugin = plugin;
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext ctx) throws CommandException {
        Optional<IMaintenanceManager> maintManager = Sponge.getServiceManager().provide(IMaintenanceManager.class);
        if (!maintManager.isPresent()) {
            plugin.getLogger().warn("Cannot toggle maintenance: IMaintenanceManager not provided!");
            throw new CommandException(Text.of("Maintenance is disabled due to an error!"));
        }
        if (!maintManager.get().isEnabled()) throw new CommandException(Text.of("Maintenance is disabled!"));
        ConfigurationNode sets = maintManager.get().getConfig().getNode("sets");

        Optional<String> optName = ctx.<String>getOne("name");
        if (!optName.isPresent()) throw new CommandException(Text.of("Set name required"));

        if (!src.hasPermission(MCSMaintenancePermissions.MAINT_SET_EDIT + "." + optName.get()))
            throw new CommandException(Text.of("You're not allowed to edit set: " + optName.get()));

        Optional<IMaintenanceSet> optSet = maintManager.get().getSet(optName.get());
        if (!optSet.isPresent()) throw new CommandException(Text.of("Set doesn't exist"));

        IMaintenanceSet set = optSet.get();
        if (!(set instanceof FNGBasicMaintenanceSet)) throw new CommandException(Text.of("Cannot edit foreign maintenance sets."));
        ConfigurationNode setNode = ((FNGBasicMaintenanceSet) set).getNode();

        Optional<String> optKey = ctx.<String>getOne("key");
        if (!optKey.isPresent()) throw new CommandException(Text.of("Missing key"));

        Optional<String> optValue = ctx.<String>getOne("value");
        if (!optKey.isPresent()) throw new CommandException(Text.of("Missing value"));
        String k = optKey.get().toLowerCase();

        switch (k) {
            case "priority":
                changeInt(setNode, optKey.get().toLowerCase(), optValue.get());
                src.sendMessage(Text.of(optName.get(), "->", optKey.get(), ":", TextColors.YELLOW, setNode.getNode(k).getInt()));
                break;
            case "allowjoin":
            case "enforceonenable":
            case "silent":
                changeBool(setNode, optKey.get().toLowerCase(), optValue.get());
                src.sendMessage(Text.of(optName.get(), "->", optKey.get(), ":", TextColors.YELLOW, setNode.getNode(k).getBoolean()));
                break;
            case "deniedworlds":
                changeList(setNode, k, optValue.get());
                break;
            case "motd":
            case "kickmessage":
            case "kickonjoinmessage":
            case "fallbackworld":
                changeText(setNode, optKey.get().toLowerCase(), optValue.get());
                src.sendMessage(Text.of(optName.get(), "->", optKey.get(), ":", TextColors.YELLOW,
                        TextSerializers.FORMATTING_CODE.deserialize(setNode.getNode(k).getString())));
                break;
            default:
                throw new CommandException(Text.of("Unknown key"));
        }
        src.sendMessage(Text.of(TextColors.GREEN, "Reloading set ..."));
        ((FNGBasicMaintenanceSet) set).reload(setNode);

        return CommandResult.success();
    }

    private void changeInt(ConfigurationNode node, String key, String value) throws CommandException {
        Integer b = 0;
        try {
            b = Integer.parseInt(value);
        } catch (Exception e) {
            throw new CommandException(Text.of("Cannot edit value"), e);
        }
        node.getNode(key.toLowerCase()).setValue(b);
    }

    private void changeBool(ConfigurationNode node, String key, String value) throws CommandException {
        boolean b = false;
        try {
            b = Boolean.parseBoolean(value);
        } catch (Exception e) {
            throw new CommandException(Text.of("Cannot edit value"), e);
        }
        node.getNode(key.toLowerCase()).setValue(b);
    }

    public void changeList(ConfigurationNode node, String key, String value) throws CommandException {
        char c = value.charAt(0);
        String s = value.substring(1);

        List<String> l = new ArrayList<String>();
        try {
            node.getNode(key).getList(TypeToken.of(String.class)).forEach(l::add);
        } catch (ObjectMappingException e) {
            throw new CommandException(Text.of("Unable to open \"" + key + "\" as list"), e);
        }
        if (c == '-') {
            if (s.equals("*")) {
                l.clear();
            } else {
                l.remove(s);
            }
        } else if (c == '+') {
            l.add(s);
        } else throw new CommandException(Text.of("Missing operator"));
        node.getNode(key).setValue(l);
    }

    public void changeText(ConfigurationNode node, String key, String value) {
        node.getNode(key).setValue(value);
    }
}
