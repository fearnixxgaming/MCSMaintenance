package de.fearnixx.sponge.mcsmaintenance.commands;

import de.fearnixx.sponge.mcsmaintenance.IMCSMaintenance;
import de.fearnixx.sponge.mcsmaintenance.IMaintenanceManager;
import de.fearnixx.sponge.mcsmaintenance.MCSMaintenancePermissions;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.Optional;

/**
 * Created by Life4YourGames on 02.12.16.
 */
public class MaintExemptCommand implements CommandExecutor {

    public static CommandSpec commandSpec(IMCSMaintenance plugin) {
        return CommandSpec.builder()
                .description(Text.of("Exempt a player from maintenance"))
                .arguments(GenericArguments.firstParsing(
                        GenericArguments.user(Text.of("user")),
                        GenericArguments.string(Text.of("userName"))
                ),
                        GenericArguments.integer(Text.of("count")))
                .executor(new MaintExemptCommand(plugin))
                .permission(MCSMaintenancePermissions.MAINT_COMM_EXEMPT_OTHER)
                .build();
    }

    private IMCSMaintenance plugin;

    public MaintExemptCommand(IMCSMaintenance plugin) {
        this.plugin = plugin;
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext ctx) throws CommandException {
        Optional<IMaintenanceManager> maintManager = Sponge.getServiceManager().provide(IMaintenanceManager.class);
        if (!maintManager.isPresent()) {
            plugin.getLogger().warn("Cannot toggle maintenance: IMaintenanceManager not provided!");
            throw new CommandException(Text.of("Maintenance is disabled due to an error!"));
        }
        if (!maintManager.get().isEnabled()) throw new CommandException(Text.of("Maintenance is disabled!"));
        Optional<User> optUser = ctx.<User>getOne("user");
        Optional<String> optName = ctx.<String>getOne("userName");
        Integer count = ctx.<Integer>getOne("count").orElse(1);

        if (!optUser.isPresent() && !optName.isPresent()) throw new CommandException(Text.of("No user(name) given"));
        //if (!count.isPresent()) throw new CommandException(Text.of("Count missing"));

        String name = optUser.map(User::getName).orElseGet(optName::get);
        if (maintManager.get().playerSetExemption(name, count)) {
            src.sendMessage(Text.of(TextColors.GOLD, name, TextColors.GREEN, " will be exempted the next ", TextColors.GOLD, count, TextColors.GREEN, " times."));
            Sponge.getServer().getOnlinePlayers().stream()
                    .filter(p -> p.hasPermission("mcsmaintenance.exempt.notify"))
                    .forEach(p -> p.sendMessage(Text.of(TextColors.GRAY, src.getName(), TextColors.YELLOW, " exempted ", TextColors.GRAY, name,
                            TextColors.YELLOW, " for a total of ", TextColors.GOLD, count, TextColors.YELLOW, " checks")));
        } else {
            throw new CommandException(Text.of("Unable to exempt \"" + name + "\""));
        }
        return CommandResult.success();
    }

}
