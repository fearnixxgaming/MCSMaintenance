package de.fearnixx.sponge.mcsmaintenance.commands;

import de.fearnixx.sponge.mcsmaintenance.IMCSMaintenance;
import de.fearnixx.sponge.mcsmaintenance.IMaintenanceManager;
import de.fearnixx.sponge.mcsmaintenance.MCSMaintenancePermissions;
import de.fearnixx.sponge.mcsmaintenance.info.FNGBasicMaintenanceSet;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.format.TextColors;

import java.util.Optional;

/**
 * Created by Life4YourGames on 23.11.16.
 */
public class MaintCreateCommand implements CommandExecutor {

    public static CommandSpec commandSpec(IMCSMaintenance plugin) {
        return CommandSpec.builder()
                .description(Text.of("Create a new maintenance set"))
                .arguments(
                        GenericArguments.string(
                                Text.of("name")
                        )
                )
                .executor(new MaintCreateCommand(plugin))
                .permission(MCSMaintenancePermissions.MAINT_SET_CREATE)
                .build();
    }

    private IMCSMaintenance plugin;

    public MaintCreateCommand(IMCSMaintenance plugin) {
        this.plugin = plugin;
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext ctx) throws CommandException {
        Optional<IMaintenanceManager> maintManager = Sponge.getServiceManager().provide(IMaintenanceManager.class);
        if (!maintManager.isPresent()) {
            plugin.getLogger().warn("Cannot toggle maintenance: IMaintenanceManager not provided!");
            throw new CommandException(Text.of("Maintenance is disabled due to an error!"));
        }
        if (!maintManager.get().isEnabled()) throw new CommandException(Text.of("Maintenance is disabled!"));
        ConfigurationNode sets = maintManager.get().getConfig().getNode("sets");

        Optional<String> optName = ctx.<String>getOne("name");
        if (!optName.isPresent()) throw new CommandException(Text.of("Set name required"));

        ConfigurationNode setNode = sets.getNode(optName.get());
        if (!setNode.isVirtual()) throw new CommandException(Text.of("Set already exists"));
        setNode.getNode("name").setValue(optName.get());
        maintManager.get().registerSet(new FNGBasicMaintenanceSet(setNode, plugin));
        src.sendMessage(Text.of(TextColors.GREEN, "Set ",
                Text.of(TextActions.showText(Text.of(TextColors.GRAY, "Click to view")),
                        TextActions.runCommand("/maint info " + optName.get()), optName.get()),
                " created successfully"));
        return CommandResult.success();
    }
}
