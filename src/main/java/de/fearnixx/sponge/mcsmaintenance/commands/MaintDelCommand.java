package de.fearnixx.sponge.mcsmaintenance.commands;

import de.fearnixx.sponge.mcsmaintenance.IMCSMaintenance;
import de.fearnixx.sponge.mcsmaintenance.IMaintenanceManager;
import de.fearnixx.sponge.mcsmaintenance.MCSMaintenancePermissions;
import de.fearnixx.sponge.mcsmaintenance.info.FNGBasicMaintenanceSet;
import de.fearnixx.sponge.mcsmaintenance.set.IMaintenanceSet;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.Optional;

/**
 * Created by Life4YourGames on 23.11.16.
 */
public class MaintDelCommand implements CommandExecutor {

    public static CommandSpec commandSpec(IMCSMaintenance plugin) {
        return CommandSpec.builder()
                .description(Text.of("Delete a maintenance set"))
                .arguments(
                        GenericArguments.string(
                                Text.of("name")
                        )
                )
                .executor(new MaintDelCommand(plugin))
                .permission(MCSMaintenancePermissions.MAINT_SET_DEL)
                .build();
    }

    private IMCSMaintenance plugin;

    public MaintDelCommand(IMCSMaintenance plugin) {
        this.plugin = plugin;
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext ctx) throws CommandException {
        Optional<IMaintenanceManager> maintManager = Sponge.getServiceManager().provide(IMaintenanceManager.class);
        if (!maintManager.isPresent()) {
            plugin.getLogger().warn("Cannot toggle maintenance: IMaintenanceManager not provided!");
            throw new CommandException(Text.of("Maintenance is disabled due to an error!"));
        }
        if (!maintManager.get().isEnabled()) throw new CommandException(Text.of("Maintenance is disabled!"));
        ConfigurationNode sets = maintManager.get().getConfig().getNode("sets");

        Optional<String> optName = ctx.<String>getOne("name");
        if (!optName.isPresent()) throw new CommandException(Text.of("Set name required"));

        if (!src.hasPermission(MCSMaintenancePermissions.MAINT_SET_DEL + "." + optName.get()))
            throw new CommandException(Text.of("You're not allowed to delete set: " + optName.get()));

        Optional<IMaintenanceSet> optSet = maintManager.get().getSet(optName.get());
        if (!optSet.isPresent()) throw new CommandException(Text.of("Set does not exist"));
        IMaintenanceSet set = optSet.get();
        if (!(set instanceof FNGBasicMaintenanceSet)) throw new CommandException(Text.of("Unable to delete foreign sets. Try disabling it"));

        maintManager.get().removeSet(set);
        src.sendMessage(Text.of(TextColors.GREEN, "Set deleted"));
        return CommandResult.success();
    }
}
