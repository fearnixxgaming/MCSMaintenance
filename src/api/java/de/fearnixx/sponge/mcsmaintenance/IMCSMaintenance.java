package de.fearnixx.sponge.mcsmaintenance;

import ninja.leaping.configurate.ConfigurationNode;
import org.slf4j.Logger;

/**
 * Created by Life4YourGames on 20.03.17.
 */
public interface IMCSMaintenance {

    ConfigurationNode getConfig();

    Logger getLogger();

    void reloadConfig();
    void saveConfig();
}
